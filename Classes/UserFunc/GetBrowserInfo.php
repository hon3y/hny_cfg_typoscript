<?php

namespace HIVE\HiveCfgTyposcript\UserFunc;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

class GetBrowserInfo
{
    public static function getBrowserInfo()
    {
        /*
        * Browser Agent
        */
        $sBrowserAgent = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('HTTP_USER_AGENT');

        $sClass = "";

        /*
         * Truncate version
         */
        DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_MAJOR);
        $dd = new DeviceDetector($sBrowserAgent);
        $dd->parse();

        /*
         * Mobile?
         */
        $bMobile = $dd->isMobile();

        /*
         * Get Os
         */
        $aOs = $dd->getOs();
        $sOs = strtolower($aOs["short_name"]);
        $sOsVersion = $aOs["version"];

        /*
         * Get browser
         */
        $aBrowser = $dd->getClient();
        $sBrowser = strtolower(str_replace(" ", "", $aBrowser["name"]));
        $sBrowserVersion = $aBrowser["version"];

        /*
         * Create Classes
         */
        $sClass .= ($sClass == '' ? '' : ' ') . "bMobile-" . ($bMobile ? '1' : '0');
        $sClass .= ($sClass == '' ? '' : ' ') . "bIOS-" . ($sOs == 'ios' ? '1' : '0');
        $sClass .= ($sClass == '' ? '' : ' ') . "bWin-" . ($sOs == 'wph' ? '1' : ($sOs == 'win' ? '1' : '0'));

        $sClass .= ($sClass == '' ? '' : ' ') . "sOs-" . $sOs;
        $sClass .= ($sClass == '' ? '' : ' ') . "sOsV-" . $sOsVersion;

        $sClass .= ($sClass == '' ? '' : ' ') . "sB-" . $sBrowser;
        $sClass .= ($sClass == '' ? '' : ' ') . "sBV-" . $sBrowserVersion;

        /*
         * Create legacy classes
         * Maybe used in older extensions
         */
        $sClass .= ($sClass == '' ? '' : ' ') . ($sOs == 'wph' ? 'win phone' : ($sOs == 'win' ? 'win' : ''));
        $sClass .= ($sClass == '' ? '' : ' ') . ($sBrowser == 'internetexplorer' ? 'ie ie' . $sBrowserVersion . ($sBrowser < 8 ? ' ieOld' : '') : '');

        return $sClass;
    }

    public static function bMobile() {
        /*
        * Browser Agent
        */
        $sBrowserAgent = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('HTTP_USER_AGENT');

        DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_MAJOR);
        $dd = new DeviceDetector($sBrowserAgent);
        $dd->parse();

        /*
         * Mobile?
         */
        $bMobile = $dd->isMobile();

        return $bMobile;
    }
}